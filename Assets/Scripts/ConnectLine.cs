﻿using UnityEngine;

public class ConnectLine : MonoBehaviour
{
    #region Properties

    public Transform StartTransform
    {
        get => startTransform;
        set => startTransform = value;
    }
    public Transform FinishTransform
    {
        get => finishTransform;
        set => finishTransform = value;
    }
    public bool IsProgress
    {
        get => isProgress;
        set => isProgress = value;
    }
    public Camera MyCamera
    {
        get => myCamera;
        set => myCamera = value;
    }
    public Vector3 Offset { get => offset; set => offset = value; }



    #endregion

    #region Private Fields

    private LineRenderer lineRenderer;
    private Transform startTransform;
    private Transform finishTransform;
    private Camera myCamera;
    private bool isProgress = true;
    private Vector3 offset;

    #endregion

    #region Unity Methods

    private void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.startWidth = 0.2f;
        lineRenderer.endWidth = 0.2f;

    }

    private void LateUpdate()
    {
        //позиционировании связи
        if (StartTransform != null)
        {
            lineRenderer.SetPosition(0, StartTransform.position);
            if (IsProgress)
            {
                if (ConnectionMethods.CurrentMethod != ConnectionMethods.Methods.first)
                {
                    Vector3 position = myCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, myCamera.WorldToScreenPoint(StartTransform.position).z));
                    lineRenderer.SetPosition(1, position);
                }
                else
                {
                    lineRenderer.SetPosition(1, StartTransform.position);
                }
            }
            else if (FinishTransform != null)
            {
                lineRenderer.SetPosition(1, FinishTransform.position);
            }
        }
    }

    #endregion
}
