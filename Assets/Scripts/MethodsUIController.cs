﻿using UnityEngine;
using UnityEngine.UI;

public class MethodsUIController : MonoBehaviour
{
    #region Unity Editor

    [SerializeField] private Toggle firstMethodToggle;
    [SerializeField] private Toggle secondMethodToggle;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        firstMethodToggle.onValueChanged.AddListener(delegate
        {
            ConnectionMethods.CurrentMethod = ConnectionMethods.Methods.first;
        });

        secondMethodToggle.onValueChanged.AddListener(delegate
        {
            ConnectionMethods.CurrentMethod = ConnectionMethods.Methods.second;
        });
    }

    #endregion
}
