﻿using UnityEngine;

public class Main : MonoBehaviour
{

    #region Unity Editor

    [SerializeField] private float Radius = 10;
    [SerializeField] private int countOfObject = 10;

    [SerializeField] private ConnectableSpawner connectableSpawner;

    #endregion

    #region Unity Methods

    private void Start()
    {
        connectableSpawner.SpawnObjects(countOfObject, Radius);
    }

    #endregion

}
