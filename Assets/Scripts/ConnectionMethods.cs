﻿using UnityEngine;

public class ConnectionMethods : MonoBehaviour
{
    #region Properties

    public static Methods CurrentMethod
    {
        get => currentMethod;
        set => currentMethod = value;
    }

    #endregion

    #region Enums

    public enum Methods
    {
        first,
        second
    }

    #endregion

    #region Private Fields

    private static Methods currentMethod = Methods.first;

    #endregion
}
