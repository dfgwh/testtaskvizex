﻿using UnityEngine;

public class ColorController : MonoBehaviour
{
    #region Properties

    public static ColorController Instance
    {
        get => instance;
        set => instance = value;
    }

    #endregion

    #region Private Fields

    private static ColorController instance = null;

    #endregion

    #region Delegate Fields

    public delegate void ColorSwich();
    public event ColorSwich onSwichColor;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }

    #endregion

    #region Public Methods

    public void SwitchColor()
    {
        onSwichColor();
    }

    #endregion

}
