﻿using UnityEngine;

public class ConnectableSpawner : MonoBehaviour
{
    #region Unity Editor

    [SerializeField] private GameObject connectablePrefab;
    [SerializeField] private Transform parentObject;

    [SerializeField] private Camera mainCamera;

    #endregion

    #region Public Methods

    /// <summary>
    /// Создание объектов
    /// реализовано с помощью x^2 + y^2 = r^2
    /// </summary>
    /// <param name="count">количество объектов</param>
    /// <param name="radius">радиус</param>
    public void SpawnObjects(int count, float radius)
    {
        for (int i = 0; i < count; i++)
        {
            float x_pos = Random.Range(-radius, radius);
            float z_pos = Random.Range(-Mathf.Sqrt(radius * radius - x_pos * x_pos), Mathf.Sqrt(radius * radius - x_pos * x_pos));
            Vector3 position = Vector3.left * x_pos
                + Vector3.forward * z_pos
                + Vector3.up * 0f;
            GameObject obj = Instantiate(connectablePrefab, position, Quaternion.identity);
            obj.transform.SetParent(parentObject);
            obj.GetComponentInChildren<ObjectMoveController>().MainCamera = mainCamera;
        }
    }

    #endregion

}
