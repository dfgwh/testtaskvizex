﻿using UnityEngine;

public class ObjectMoveController : MonoBehaviour
{
    #region Properties

    public Camera MainCamera
    {
        get => mainCamera;
        set => mainCamera = value;
    }

    #endregion

    #region Unity Editor

    [SerializeField] private Transform mainBody;

    #endregion

    #region Private Fields

    private Camera mainCamera;
    private float zPosition;
    private Vector3 offset;
    private bool isMove;

    #endregion

    #region Unity Methods

    private void Start()
    {
        zPosition = mainCamera.WorldToScreenPoint(transform.position).z;
    }

    private void Update()
    {
        if (isMove)
        {
            Vector3 position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, zPosition);
            mainBody.position = mainCamera.ScreenToWorldPoint(position + new Vector3(offset.x, offset.y));
        }
    }

    private void OnMouseDown()
    {
        if (!isMove)
        {
            StartMove();
        }
    }

    private void OnMouseUp()
    {
        FinishMove();
    }


    #endregion

    #region Private Methods

    /// <summary>
    /// Начало движения объекта
    /// </summary>
    private void StartMove()
    {
        isMove = true;
        offset = mainCamera.WorldToScreenPoint(transform.position) - Input.mousePosition;
    }

    /// <summary>
    /// Конец движения объекта
    /// </summary>
    private void FinishMove()
    {
        isMove = false;
    }

    #endregion
}
