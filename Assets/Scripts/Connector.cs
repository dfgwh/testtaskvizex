﻿using UnityEngine;

public class Connector : MonoBehaviour
{
    #region Unity Editor

    [SerializeField] private Color selecteColor;
    [SerializeField] private Color otherColor;

    #endregion

    #region Private Fields

    private Color startColor;
    private Renderer myRenderer;
    private bool isSwitch = false;


    #endregion

    #region Unity Methods

    private void Start()
    {
        myRenderer = GetComponent<Renderer>();
        startColor = myRenderer.material.color;
        ColorController.Instance.onSwichColor += Instance_onSwichColor;
    }

    private void OnDestroy()
    {
        ColorController.Instance.onSwichColor -= Instance_onSwichColor;
    }

    private void OnMouseDown()
    {
        if (ConnectionMethods.CurrentMethod == ConnectionMethods.Methods.first)
        {
            if (!ConnectLineController.Instance.IsLine)
            {
                ConnectLineController.Instance.StartLine(transform);
                
                myRenderer.material.color = selecteColor;
            }
            else
            {
                ConnectLineController.Instance.StopLine(transform);
            }
        }
        else
        {
            if (!ConnectLineController.Instance.IsLine)
            {
                ConnectLineController.Instance.StartLine(transform);

                myRenderer.material.color = selecteColor;
            }
        }
    }

    
    private void OnMouseUp()
    {
        if (ConnectionMethods.CurrentMethod == ConnectionMethods.Methods.second)
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit))
            {
                if (hit.transform.tag == transform.tag) //проверяем, что кнопка отпущена на сфере
                {
                    ConnectLineController.Instance.StopLine(hit.transform);
                }
                else
                {
                    ConnectLineController.Instance.DeleteLine();
                }
            }
            else
            {
                ConnectLineController.Instance.DeleteLine();
            }
        }
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// смена цвета
    /// переключает стартовый цвет на "синий" и наоборот
    /// </summary>
    private void Instance_onSwichColor()
    {
        if (!isSwitch)
        {
            myRenderer.material.color = otherColor;
            isSwitch = true;
        }
        else
        {
            myRenderer.material.color = startColor;
            isSwitch = false;
        }
    }

    #endregion
}
