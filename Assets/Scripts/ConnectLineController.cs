﻿using UnityEngine;

public class ConnectLineController : MonoBehaviour
{
    #region Constants

    private const string CONNECTOR_TAG = "connector";

    #endregion

    #region Properties

    public static ConnectLineController Instance
    {
        get => instance;
        set => instance = value;
    }
    public bool IsLine { get => isLine; set => isLine = value; }

    #endregion

    #region Unity Editor

    [SerializeField] private Camera myCamera;
    [SerializeField] private GameObject linePrefab;

    #endregion

    #region Private Fields

    private static ConnectLineController instance = null;
    private ConnectLine currentConnectLine = null;
    private bool isLine = false;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        if (Instance==null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }

    private void Update()
    {
        // Проверка для первого способа на клик по пустому пространству и на другие объекты
        if (ConnectionMethods.CurrentMethod == ConnectionMethods.Methods.first)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit))
                {
                    if (hit.transform.tag != CONNECTOR_TAG && currentConnectLine)
                    {
                        DeleteLine();
                    }
                }
                else if (currentConnectLine)
                {
                    DeleteLine();
                }
            }
        }
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Начало выбора конца связи
    /// Создаем связь, прикрепляем к начальной точке
    /// </summary>
    /// <param name="startTransform">начальная точка</param>
    public void StartLine(Transform startTransform)
    {
        isLine = true;

        var obj = Instantiate(linePrefab, transform.position, Quaternion.identity) as GameObject;
        obj.transform.SetParent(transform);

        currentConnectLine = obj.GetComponent<ConnectLine>();
        currentConnectLine.Offset = myCamera.WorldToScreenPoint(startTransform.position) - Input.mousePosition;
        currentConnectLine.MyCamera = myCamera;
        currentConnectLine.StartTransform = startTransform;
        currentConnectLine.IsProgress = true;

        ColorController.Instance.SwitchColor();
    }

    /// <summary>
    /// Остановка выбора конца связи
    /// Прикрепление к конечной точке или если совпадает начальная и конечные точки - удаление связи
    /// </summary>
    /// <param name="finishTransform">конечная точка</param>
    public void StopLine(Transform finishTransform)
    {
        if (finishTransform.position == currentConnectLine.StartTransform.position)
        {
            DeleteLine();
        }
        else
        {
            currentConnectLine.FinishTransform = finishTransform;
            currentConnectLine.IsProgress = false;
            currentConnectLine = null;
            isLine = false;

            ColorController.Instance.SwitchColor();
        }
    }

    /// <summary>
    /// Удаление связи
    /// </summary>
    public void DeleteLine()
    {
        Destroy(currentConnectLine.gameObject);
        currentConnectLine = null;
        isLine = false;
        ColorController.Instance.SwitchColor();
    }

    #endregion
}
